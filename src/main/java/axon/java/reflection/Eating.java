package axon.java.reflection;

public interface Eating {
    String eats();
}
