package axon.java.reflection;

public interface Locomotion {
    String getLocomotion();
}
