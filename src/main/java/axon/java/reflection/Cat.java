package axon.java.reflection;

public class Cat extends Animal implements Locomotion {

    public Cat(String name) {
        super(name);
    }

    @Override
    protected String getSound() {
        return "bleat";
    }

    @Override
    public String getLocomotion() {
        return "walks";
    }

    @Override
    public String eats() {
        return "grass";
    }

    public void myCatMethod(){

    }
}
