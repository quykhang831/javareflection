import axon.java.reflection.Animal;
import axon.java.reflection.Cat;
import org.junit.jupiter.api.Test;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CatTest {

    // Class Names
    @Test
    public void givenObject_whenGetsClassName_thenCorrect() {
        Object cat = new Cat("Kitty");
        Class<?> catClass = cat.getClass();

        assertEquals("Cat", catClass.getSimpleName());
        assertEquals("axon.java.reflection.Cat", catClass.getName());
    }

    @Test
    public void givenClassName_whenCreatesObject_thenCorrect() throws ClassNotFoundException {
        Class<?> catClass = Class.forName("axon.java.reflection.Cat");

        assertEquals("Cat", catClass.getSimpleName());
        assertEquals("axon.java.reflection.Cat", catClass.getName());
    }

    // Package Information
    @Test
    public void givenClass_whenGetsPackageInfo_thenCorrect() {
        Cat cat = new Cat("Kitty");
        Class<?> catClass = cat.getClass();
        Package pkg = catClass.getPackage();

        assertEquals("axon.java.reflection", pkg.getName());
    }

    // Class Modifiers
    @Test
    public void givenClass_whenRecognisesModifiers_thenCorrect() throws ClassNotFoundException {
        Class<?> catClass = Class.forName("axon.java.reflection.Cat");
        Class<?> animalClass = Class.forName("axon.java.reflection.Animal");

        int catMods = catClass.getModifiers();
        int animalMods = animalClass.getModifiers();

        assertTrue(Modifier.isPublic(catMods));
        assertTrue(Modifier.isAbstract(animalMods));
        assertTrue(Modifier.isPublic(animalMods));
    }

    // Superclass
    @Test
    public void givenClass_whenGetsSuperClass_thenCorrect() {
        Cat cat = new Cat("Kitty");
        String str = "any string";

        Class<?> catClass = cat.getClass();
        Class<?> catSuperClass = catClass.getSuperclass();

        assertEquals("Animal", catSuperClass.getSimpleName());
        assertEquals("Object", str.getClass().getSuperclass().getSimpleName());
    }

    // Implemented Interfaces
    @Test
    public void givenClass_whenGetsImplementedInterfaces_thenCorrect() throws ClassNotFoundException {
        Class<?> catClass = Class.forName("axon.java.reflection.Cat");
        Class<?> animalClass = Class.forName("axon.java.reflection.Animal");

        Class<?>[] catInterfaces = catClass.getInterfaces();
        Class<?>[] animalInterfaces = animalClass.getInterfaces();

        assertEquals(1, catInterfaces.length);
        assertEquals(1, animalInterfaces.length);
        assertEquals("Locomotion", catInterfaces[0].getSimpleName());
        assertEquals("Eating", animalInterfaces[0].getSimpleName());
    }

    // Constructors, Methods and Fields
    @Test
    public void givenClass_whenGetsConstructor_thenCorrect() throws ClassNotFoundException {
        Class<?> catClass = Class.forName("axon.java.reflection.Cat");

        Constructor<?>[] constructors = catClass.getConstructors();

        assertEquals(1, constructors.length);
        assertEquals("axon.java.reflection.Cat", constructors[0].getName());
    }

    @Test
    public void givenClass_whenGetsFields_thenCorrect() throws ClassNotFoundException {
        Class<?> animalClass = Class.forName("axon.java.reflection.Animal");
        Field[] fields = animalClass.getDeclaredFields();

        List<String> actualFields = getFieldNames(fields);

        assertEquals(2, actualFields.size());
        assertTrue(actualFields.containsAll(Arrays.asList("name", "CATEGORY")));
    }

    @Test
    public void givenClass_whenUpdatesFields_thenCorrect() throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        Class<?> animalClass = Class.forName("axon.java.reflection.Animal");
        Cat tom = new Cat("Tom");

        Field catNameField = animalClass.getDeclaredField("name");
        catNameField.setAccessible(true);
        catNameField.set(tom, "Thomas");
        assertEquals("Thomas", tom.getName());
    }

    @Test
    public void givenClass_whenGetsMethods_thenCorrect() throws ClassNotFoundException {
        Class<?> animalClass = Class.forName("axon.java.reflection.Animal");
        Method[] methods = animalClass.getDeclaredMethods();
        List<String> actualMethods = getMethodNames(methods);

        assertEquals(5, actualMethods.size());
        assertTrue(actualMethods.containsAll(Arrays.asList("getName",
                "setName", "getSound", "getCATEGORY", "setCATEGORY")));
    }

    @Test
    public void givenClass_whenUpdatesFieldsByInvokingMethods_thenCorrect() throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        Class<?> animalClass = Class.forName("axon.java.reflection.Animal");
        Cat tom = new Cat("Tom");

        Method setCatNameMethod = animalClass.getMethod("setName", String.class);
        setCatNameMethod.invoke(tom, "Thomas");
        assertEquals("Thomas", tom.getName());
    }

    private static List<String> getFieldNames(Field[] fields) {
        List<String> fieldNames = new ArrayList<>();
        for (Field field : fields)
            fieldNames.add(field.getName());
        return fieldNames;
    }

    private static List<String> getMethodNames(Method[] methods) {
        List<String> methodNames = new ArrayList<>();
        for (Method method : methods)
            methodNames.add(method.getName());
        return methodNames;
    }
}
